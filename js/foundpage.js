function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function() {
                    alert('success');
                  }, function(error) {
                    alert('fail');
                  });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    var btnUid = document.getElementById('btnUid');
    var frienduid = document.getElementById('frienduid');
    btnUid.addEventListener('click', function () {
        loginUser = firebase.auth().currentUser;
        console.log("登入使用者為",loginUser);
        firebase.database().ref('friends/' + loginUser.uid).set({
            email: loginUser.email,
            friend_uid: frienduid.value,
        });        
    });
}

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
        console.log('log in');
    } else {
      // No user is signed in.
      console.log('not logged in');
    }
  });

window.onload = function () {
    init();
};
