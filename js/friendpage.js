function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function() {
                    alert('success');
                  }, function(error) {
                    alert('fail');
                  });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
}
var storageRef = firebase.storage().ref();
var reader  = new FileReader();
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
        var friend;
        loginUser=firebase.auth().currentUser;
        firebase.database().ref('friends/' + loginUser.uid).once('value').then(function(snapshot) {
            var childData = snapshot.val();
			friend=childData.friend_uid;   
        //});
            firebase.database().ref('users/' + friend).once('value').then(function(snapshot) {
            ///window.open(url, "_self");
                storageRef.child(snapshot.val().picture).getDownloadURL().then(function (url) {
                    // `url` is the download URL for fileRefDL.value
                    function getsrc(){
                        document.getElementById("faa").src=url;
                    }
                    getsrc();
                }).catch(function (error) {
                    console.log(error.message);
                });

                var total_inf = [];
                var childData = snapshot.val();
                total_inf[total_inf.length]="name:"+childData.name+"<br>"+"email:"+childData.email+"<br>"+"gender:"+childData.gender+"<br>"+"career:"+childData.career+"<br>"+"uid:"+childData.uid;
                document.getElementById('finf_list').innerHTML=total_inf.join('');    
            });

            var str_before_username = "<div class='my-3 p-3 rounded box-shadow' style='background-color:#FFFAF2'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
            var str_after_content = "</p></div></div>\n";
        
            var postsRef = firebase.database().ref('mypost/'+friend);
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
        
            postsRef.once('value')
                .then(function (snapshot) {
                        snapshot.forEach(function(childSnapshot){
                        var childData = childSnapshot.val();
                        total_post[total_post.length]=str_before_username+childData.data+str_after_content;
                        first_count+=1;
                    });
                    document.getElementById('fper_post_list').innerHTML=total_post.join('');
                })
                .catch(e => console.log(e.message));

        });
    } else {
      // No user is signed in.
      console.log('not logged in');
    }
  });

window.onload = function () {
    init();
};
