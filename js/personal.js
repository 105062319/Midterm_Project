function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function() {
                    alert('success');
                  }, function(error) {
                    alert('fail');
                  });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('inf_list').innerHTML = "";
        }
    });
}
var storageRef = firebase.storage().ref();
var reader  = new FileReader();
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.
        loginUser=firebase.auth().currentUser;
        firebase.database().ref('users/' + loginUser.uid).once('value').then(function(snapshot) {
        ///window.open(url, "_self");
            storageRef.child(snapshot.val().picture).getDownloadURL().then(function (url) {
                // `url` is the download URL for fileRefDL.value
                function getsrc(){
                    document.getElementById("aa").src=url;
                }
                getsrc();
            }).catch(function (error) {
                console.log(error.message);
            });

            var total_inf = [];
            var childData = snapshot.val();
            total_inf[total_inf.length]="name:"+childData.name+"<br>"+"email:"+childData.email+"<br>"+"gender:"+childData.gender+"<br>"+"career:"+childData.career+"<br>"+"uid:"+childData.uid;
            document.getElementById('inf_list').innerHTML=total_inf.join('');    
        });

        post_btn = document.getElementById('per_post_btn');
        post_txt = document.getElementById('per_comment');
    
        post_btn.addEventListener('click', function () {
            if (post_txt.value != "") {
                firebase.database().ref('mypost/'+loginUser.uid).push().set({data:post_txt.value});
                post_txt.value=null;
            }
        });
    
        // The html code for post
        var str_before_username = "<div class='my-3 p-3 rounded box-shadow' style='background-color:#FFFAF2'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
        var str_after_content = "</p></div></div>\n";
    
        var postsRef = firebase.database().ref('mypost/'+loginUser.uid);
        // List for store posts html
        var total_post = [];
        // Counter for checking history post update complete
        var first_count = 0;
        // Counter for checking when to update new post
        var second_count = 0;
    
        postsRef.once('value')
            .then(function (snapshot) {
                /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
                ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
                ///         2. Join all post in list to html in once
                ///         4. Add listener for update the new post
                ///         5. Push new post's html to a list
                ///         6. Re-join all post in list to html when update
                ///
                ///         Hint: When history post count is less then new post count, update the new and refresh html
                snapshot.forEach(function(childSnapshot){
                    var childData = childSnapshot.val();
                    total_post[total_post.length]=str_before_username +childData.data+str_after_content;
                    first_count+=1;
                });
                document.getElementById('per_post_list').innerHTML=total_post.join('');
    
                postsRef.on('child_added',function(data){
                    second_count+=1;
                    if(second_count>first_count){
                        var childData = data.val();
                        total_post[total_post.length]= str_before_username+ childData.data+str_after_content;
                        document.getElementById('per_post_list').innerHTML=total_post.join('');
                    }
                });
            })
            .catch(e => console.log(e.message));
    } else {
      // No user is signed in.
      console.log('not logged in');
    }
  });

window.onload = function () {
    init();
};
