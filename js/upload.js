function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function() {
                    alert('success');
                  }, function(error) {
                    alert('fail');
                  });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    
    // Create a storage reference from our storage service
    var storageRef = firebase.storage().ref();

    // Upload
    ///var selectedFile;
    var fileRefUL = document.getElementById('fileRefUL');
    var btnUL = document.getElementById('btnUL');

    btnUL.addEventListener('click', e => {
        var fileRef = storageRef.child(fileRefUL.value);
        fileRef.put(selectedFile).then(function (snapshot) {
            console.log('Uploaded a blob or file!');
        }).catch(function (error) {
            console.log(error.message);
        });
    });

    var fileRefDL = document.getElementById('fileRefDL');
    var btnDL = document.getElementById('btnDL');

    btnDL.addEventListener('click', e => {
        storageRef.child(fileRefDL.value).getDownloadURL().then(function (url) {
            // `url` is the download URL for fileRefDL.value
            window.open(url, "_self");  
        }).catch(function (error) {
            console.log(error.message);
        });
    });

    
}

window.onload = function () {
    init();
};
