# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Simple forum
* Key functions (add/delete)
    1. user page
    2. post page
* Other functions (add/delete)
    None

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Not all|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
最主要的功能是聊天，需要登入後才能使用。
主畫面的Simple Forum框框使用了css animation功能，底色會改變。
UploadPage可以上傳照片，照片的Reference可以在填寫PersonalInf時使用，會成為PersonalPage的圖片。
PersonalInf是用來填寫個人資料的頁面。
PersonalPage則會顯現出上一次在PersonalInf填寫的資料，還有個人的uid，並且有發布個人想法的post功能，他人可以看見。
FoundFriend中輸入別人的uid再點Found後，點擊FriendPage即可看見他人的PersonalPage。
除了登入以外的頁面，普通游標皆為一個小人的頭。
登入頁面可使用Google登入或是創辦帳號。
為了實現RWD，將圖片的height設為auto，max-weight設為80%，而文字則會自動斷行。
## Security Report (Optional)
None